#include <chrono>
#include <cstdlib>
#include <functional>
#include <memory>
#include <string>
#include <cmath>
#include <fstream>
#include <ctime>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "std_msgs/msg/string.hpp"
#include "nav2_msgs/action/navigate_to_pose.hpp"
#include "nav2_util/geometry_utils.hpp"
#include "tf2_msgs/msg/tf_message.hpp"
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2/utils.h"
#include <tf2/exceptions.h>
#include "nav2_util/geometry_utils.hpp"
#include "nav2_util/robot_utils.hpp"

using namespace std::chrono_literals;

float g_feedback_distance = 0.0;
bool g_isNavigating = false;

class FourPointNav : public rclcpp::Node
{
public:
    using NavigateToPose = nav2_msgs::action::NavigateToPose;
    using GoalHandleNavigateToPose = rclcpp_action::ClientGoalHandle<NavigateToPose>;

    FourPointNav() : Node("four_point_nav")
    {
        navigation_action_client_ = rclcpp_action::create_client<NavigateToPose>(this, "navigate_to_pose");

        // List of target poses
        std::vector<geometry_msgs::msg::PoseStamped> poses = {
            createPose(2.0, 0.0, 0.0, 0.0),
            createPose(2.0, 1.0, 0.0, 0.0),
            createPose(0.0, 1.0, 0.0, 0.0),
            createPose(0.0, 0.0, 0.0, 0.0)
        };

        // Call the sendGoals function with poses as your argument
    }

private:
    rclcpp_action::Client<NavigateToPose>::SharedPtr navigation_action_client_;
    rclcpp_action::ClientGoalHandle<NavigateToPose>::SharedPtr navigation_goal_handle_;

    void sendGoals(/*your code here*/)
    {
        for (const auto& pose : poses) {
            // Check if navigation action server is ready
            // Write your code here
            ////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////  

            NavigateToPose::Goal navigation_goal_;
            navigation_goal_.pose = pose;

            // Feedback when the robot is navigating
            auto send_goal_options = rclcpp_action::Client<NavigateToPose>::SendGoalOptions();

            // Fill up the callbacks
            send_goal_options.goal_response_callback = std::bind(&FourPointNav::/*your code here*/, this, std::placeholders::_1);
            send_goal_options.feedback_callback = std::bind(&FourPointNav::/*your code here*/, this, std::placeholders::_1, std::placeholders::_2);
            send_goal_options.result_callback = std::bind(&FourPointNav::/*your code here*/, this, std::placeholders::_1);

            // Send the goal to the navigation action client and set logic
            // Write your code here
            ////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////   
            g_isNavigating = /*True or False*/;
            

            // Wait for the current goal to complete before sending the next one
            while (g_isNavigating) {
                rclcpp::spin_some(this->get_node_base_interface());
            }
        }
    }

    void goal_response_callback(std::shared_future<GoalHandleNavigateToPose::SharedPtr> goal_handle)
    {
        if (!goal_handle.get()) {
            RCLCPP_ERROR(this->get_logger(), "Goal was rejected by the server");
        } else {
            RCLCPP_INFO(this->get_logger(), "Goal accepted by the server, waiting for the result");
        }
    }

    void feedback_callback(GoalHandleNavigateToPose::SharedPtr, const std::shared_ptr<const NavigateToPose::Feedback> feedback)
    {
        g_feedback_distance = feedback->distance_remaining;
        std::string feedback_dist_msg = "Distance remaining: " + std::to_string(g_feedback_distance);
        RCLCPP_INFO(this->get_logger(), feedback_dist_msg.c_str());
    }

    void result_callback(const rclcpp_action::ClientGoalHandle<NavigateToPose>::WrappedResult& result)
    {
        if (result.code == rclcpp_action::ResultCode::SUCCEEDED) {
            RCLCPP_INFO(this->get_logger(), "Navigation goal succeeded");
        } else {
            RCLCPP_INFO(this->get_logger(), "Navigation goal failed");
        }
        g_isNavigating = false;
    }

    geometry_msgs::msg::PoseStamped createPose(double x, double y, double z, double orientation)
    {
        // Write your code here
        ////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////
        return pose;
    }
};

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<FourPointNav>());
    rclcpp::shutdown();
    return 0;
}
